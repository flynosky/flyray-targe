package me.flyray.crm.core.modules.platform;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.core.entity.PlatformCallbackUrl;
import me.flyray.crm.core.biz.platform.PlatformCallbackUrlBiz;
import me.flyray.crm.facade.request.CallBackUrlRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("platformCallbackUrl")
public class PlatformCallbackUrlController extends BaseController<PlatformCallbackUrlBiz, PlatformCallbackUrl> {

	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<PlatformCallbackUrl> pageList(@RequestBody CallBackUrlRequest bean){
		bean.setPlatformId(setPlatformId(bean.getPlatformId()));
        return baseBiz.callbackUrlList(bean);
    }
	
	@RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> add(@RequestBody CallBackUrlRequest bean) throws Exception{
        baseBiz.addCallbackUrl(bean);
        return ResponseEntity.ok(bean);
    }
	
	@ApiOperation("修改平台/商户回调地址")
	@RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> update(@RequestBody @Valid CallBackUrlRequest bean) throws Exception {
		Map<String, Object> response = baseBiz.updateCallbackUrl(bean);
		return response;
    }
	
	@ApiOperation("删除平台/商户回调地址")
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> delete(@RequestBody @Valid CallBackUrlRequest bean) throws Exception {
		Map<String, Object> response = baseBiz.deleteCallbackUrl(bean);
		return response;
    }
}