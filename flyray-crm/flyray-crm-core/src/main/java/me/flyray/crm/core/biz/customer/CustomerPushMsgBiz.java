package me.flyray.crm.core.biz.customer;

import me.flyray.crm.core.entity.CustomerPushMsg;
import me.flyray.crm.core.mapper.CustomerPushMsgMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 客户消息推送参数表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Service
public class CustomerPushMsgBiz extends BaseBiz<CustomerPushMsgMapper, CustomerPushMsg> {
}