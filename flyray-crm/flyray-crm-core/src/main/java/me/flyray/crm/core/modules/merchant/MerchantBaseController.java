package me.flyray.crm.core.modules.merchant;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.MerchantBase;
import me.flyray.crm.facade.request.MerchantBaseRequest;
import me.flyray.crm.facade.request.QueryMerchantBaseListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.ResponseHelper;
import me.flyray.crm.core.biz.merchant.MerchantBaseBiz;

/**
 * 商户客户基础信息
 * @author centerroot
 * @time 创建时间:2018年7月16日下午6:09:38
 * @description
 */
@RestController
@RequestMapping("merchantBase")
public class MerchantBaseController extends BaseController<MerchantBaseBiz, MerchantBase> {
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:04
	 * @param queryMerchantBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid QueryMerchantBaseListRequest queryMerchantBaseListRequest){
		queryMerchantBaseListRequest.setPlatformId(setPlatformId(queryMerchantBaseListRequest.getPlatformId()));
		return merchantBaseBiz.queryList(queryMerchantBaseListRequest);
	}
	
	/**
	 * 添加商户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param MerchantBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid MerchantBaseRequest MerchantBaseRequest){
		return merchantBaseBiz.add(MerchantBaseRequest);
	}
	/**
	 * 查询单个客户基本信息
	 */
	@RequestMapping(value = "/info/{merchantId}", method = RequestMethod.GET)
    @ResponseBody
	public MerchantBase queryInfo(@PathVariable String merchantId){
		return merchantBaseBiz.queryInfo(merchantId);
	}
	
	/**
	 * 根据序号删除客户基本信息
	 * @author centerroot
	 * @time 创建时间:2018年8月24日下午2:45:07
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteOne/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> deleteOne(@PathVariable Integer id){
		Map<String, Object> respMap = baseBiz.deleteOne(id);
        return respMap;
    }
	
	/**
	 * 查询商户是否设置交易密码
	 * @param merchantBaseRequest
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getMerchantInfo", method = RequestMethod.POST)
	public Map<String, Object> queryMerchantPassword(@RequestBody MerchantBaseRequest merchantBaseRequest){
		
		Map<String, Object> beanToMap = EntityUtils.beanToMap(merchantBaseRequest);
		CustomerBase customerBase = merchantBaseBiz.queryMerchantPassword(beanToMap);
		if(null == customerBase){
			return ResponseHelper.success(null, null, BizResponseCode.MER_NOTEXIST.getCode(), BizResponseCode.MER_NOTEXIST.getMessage());
		}else{
			return ResponseHelper.success(customerBase, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
		}
	}
	
}