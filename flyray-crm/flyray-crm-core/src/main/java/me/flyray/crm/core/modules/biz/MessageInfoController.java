package me.flyray.crm.core.modules.biz;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.MessageInfoBiz;
import me.flyray.crm.core.entity.MessageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("messageInfo")
public class MessageInfoController extends BaseController<MessageInfoBiz, MessageInfo> {

}