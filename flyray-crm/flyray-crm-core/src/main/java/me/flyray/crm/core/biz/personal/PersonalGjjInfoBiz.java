package me.flyray.crm.core.biz.personal;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.core.mapper.PersonalGjjMapper;
import me.flyray.crm.core.entity.PersonalGjjInfos;
import me.flyray.crm.facade.request.PersonalGjjInfoReq;
import me.flyray.crm.facade.response.BaseResponse;
import me.flyray.crm.facade.response.PersonalGjjInfoRsp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import me.flyray.common.biz.BaseBiz;
import me.flyray.common.util.EntityUtils;

/**
 * 公积金基本信息管理
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PersonalGjjInfoBiz extends BaseBiz<PersonalBaseMapper, PersonalBase> {

	private final static Logger logger = LoggerFactory.getLogger(PersonalGjjInfoBiz.class);
	@Autowired
	private PersonalGjjMapper personalGjjMapper;

	@Autowired
	private PersonalBaseMapper personalBaseMapper;

	/**
	 * 新增账户信息
	 * 
	 */
	public BaseResponse insertGjjInfo(PersonalGjjInfoReq params) {
		logger.info("用户注册请求参数：{}" + EntityUtils.beanToMap(params));
		BaseResponse response = new BaseResponse();
		//手机号为空时只验证合作平台编号和会员号
		PersonalGjjInfos pginfo=new PersonalGjjInfos();
		//平台编号
		pginfo.setPlatformId(params.getPersonalId());
		//身份证号
		pginfo.setSpidNo(params.getSpidNo());;
		PersonalGjjInfos pginfos=personalGjjMapper.selectOne(pginfo);
		if (null != pginfos) {
			response.setCode(BizResponseCode.USERINFO_ISEXIST.getCode());
			response.setMessage(BizResponseCode.REGISTER_ERROR.getMessage());
			return response;
		}
		pginfos = new PersonalGjjInfos();
		if(!StringUtils.isEmpty(params.getPersonalId())){
			pginfos.setPersonalId(params.getPersonalId());
		}else{
			PersonalBase custInfo= new PersonalBase();
			custInfo.setIdCard(params.getSpidNo());
			PersonalBase custInfos=personalBaseMapper.selectOne(custInfo);
			if(null!=custInfos){
				pginfos.setPersonalId(custInfos.getPersonalId());
			}
		}
		if(!StringUtils.isEmpty(params.getPlatformId())){
			pginfos.setPlatformId(params.getPlatformId());
		}
		if(!StringUtils.isEmpty(params.getSpname())){
			pginfos.setSpname(params.getSpname());
		}
		if (!StringUtils.isEmpty(params.getSex())) {
			pginfos.setSex(params.getSex());
		}
		if (!StringUtils.isEmpty(params.getSpidNo())) {
			pginfos.setSpidNo(params.getSpidNo());
		}
		if (!StringUtils.isEmpty(params.getZhzt())) {
			pginfos.setZhzt(params.getZhzt());
		}
		if (!StringUtils.isEmpty(params.getSjh())) {
			pginfos.setSjh(params.getSjh());
		}
		if (!StringUtils.isEmpty(params.getOrgName())) {
			pginfos.setOrgName(params.getOrgName());
		}
		if (!StringUtils.isEmpty(params.getSnCode())) {
			pginfos.setSnCode(params.getSnCode());
		}
		if (!StringUtils.isEmpty(params.getSnName())) {
			pginfos.setSnName(params.getSnName());
		}
		if (!StringUtils.isEmpty(params.getSpCode())) {
			pginfos.setSpCode(params.getSpCode());
		}
		if (!StringUtils.isEmpty(params.getKhrq())) {
			pginfos.setKhrq(params.getKhrq());
		}
		if (!StringUtils.isEmpty(params.getSpgz())) {
			pginfos.setSpgz(params.getSpgz());
		}
		if (!StringUtils.isEmpty(params.getSpsingl())) {
			pginfos.setSpsingl(params.getSpsingl());
		}
		if (!StringUtils.isEmpty(params.getSpjcbl())) {
			pginfos.setSpjcbl(params.getSpjcbl());
		}
		if (!StringUtils.isEmpty(params.getSpmfact())) {
			pginfos.setSpmfact(params.getSpmfact());
		}
		if (!StringUtils.isEmpty(params.getSpmend())) {
			pginfos.setSpmend(params.getSpmend());
		}
		if (!StringUtils.isEmpty(params.getSpjym())) {
			pginfos.setSpjym(params.getSpjym());
		}
		int isinsertok = personalGjjMapper.insertSelective(pginfos);
		if (isinsertok > 0) {
			response.setCode(BizResponseCode.OK.getCode());
			response.setMessage(BizResponseCode.OK.getMessage());
			return response;
		}
		response.setCode(BizResponseCode.GJJ_INSERT_ERROR.getCode());
		response.setMessage(BizResponseCode.GJJ_INSERT_ERROR.getMessage());
		logger.info("用户注册返回参数：{}" + EntityUtils.beanToMap(response));
		return response;
		
	}

	/***
	 * 
	 * 修改用户信息
	 */
	public BaseResponse updateGjjInfo(PersonalGjjInfoReq params) {
		logger.info("修改用户信息请求参数：{}" + EntityUtils.beanToMap(params));
		BaseResponse response = new BaseResponse();
		//手机号为空时只验证合作平台编号和会员号
		PersonalGjjInfos pginfo=new PersonalGjjInfos();
		//平台编号
		pginfo.setPlatformId(params.getPersonalId());
		//身份证号
		pginfo.setSpidNo(params.getSpidNo());
		PersonalGjjInfos pginfos=personalGjjMapper.selectOne(pginfo);
		if (null == pginfos) {// 修改的用户信息不存在
			response.setCode(BizResponseCode.MEMBERMODIFY_NULL_CODE.getCode());
			response.setMessage(BizResponseCode.MEMBERMODIFY_NULL_CODE.getMessage());
			return response;
		}
		if(!StringUtils.isEmpty(params.getPersonalId())){
			pginfos.setPersonalId(params.getPersonalId());
		}else{
			PersonalBase custInfo= new PersonalBase();
			custInfo.setIdCard(params.getSpidNo());
			PersonalBase custInfos=personalBaseMapper.selectOne(custInfo);
			if(null!=custInfos){
				pginfos.setPersonalId(custInfos.getPersonalId());
			}
		}
		if(!StringUtils.isEmpty(params.getPlatformId())){
			pginfos.setPlatformId(params.getPlatformId());
		}
		if(!StringUtils.isEmpty(params.getSpname())){
			pginfos.setSpname(params.getSpname());
		}
		if (!StringUtils.isEmpty(params.getSex())) {
			pginfos.setSex(params.getSex());
		}
		if (!StringUtils.isEmpty(params.getSpidNo())) {
			pginfos.setSpidNo(params.getSpidNo());
		}
		if (!StringUtils.isEmpty(params.getZhzt())) {
			pginfos.setZhzt(params.getZhzt());
		}
		if (!StringUtils.isEmpty(params.getSjh())) {
			pginfos.setSjh(params.getSjh());
		}
		if (!StringUtils.isEmpty(params.getOrgName())) {
			pginfos.setOrgName(params.getOrgName());
		}
		if (!StringUtils.isEmpty(params.getSnCode())) {
			pginfos.setSnCode(params.getSnCode());
		}
		if (!StringUtils.isEmpty(params.getSnName())) {
			pginfos.setSnName(params.getSnName());
		}
		if (!StringUtils.isEmpty(params.getSpCode())) {
			pginfos.setSpCode(params.getSpCode());
		}
		if (!StringUtils.isEmpty(params.getKhrq())) {
			pginfos.setKhrq(params.getKhrq());
		}
		if (!StringUtils.isEmpty(params.getSpgz())) {
			pginfos.setSpgz(params.getSpgz());
		}
		if (!StringUtils.isEmpty(params.getSpsingl())) {
			pginfos.setSpsingl(params.getSpsingl());
		}
		if (!StringUtils.isEmpty(params.getSpjcbl())) {
			pginfos.setSpjcbl(params.getSpjcbl());
		}
		if (!StringUtils.isEmpty(params.getSpmfact())) {
			pginfos.setSpmfact(params.getSpmfact());
		}
		if (!StringUtils.isEmpty(params.getSpmend())) {
			pginfos.setSpmend(params.getSpmend());
		}
		if (!StringUtils.isEmpty(params.getSpjym())) {
			pginfos.setSpjym(params.getSpjym());
		}
		int isupdatok = personalGjjMapper.updateByPrimaryKeySelective(pginfos);
		if (isupdatok > 0) {
			response.setCode(BizResponseCode.OK.getCode());
			response.setMessage(BizResponseCode.OK.getMessage());
			return response;
		}
		response.setCode(BizResponseCode.GJJ_UPDATE_ERROR.getCode());
		response.setMessage(BizResponseCode.GJJ_UPDATE_ERROR.getMessage());
		logger.info("修改用户信息返回参数：{}" + EntityUtils.beanToMap(response));
		return response;
	}
	
	/**
	 * 查询用户信息
	 **/
	public PersonalGjjInfoRsp personalGjjQuery(PersonalGjjInfoReq params) {
		logger.info("查询用户信息请求参数：{}" + EntityUtils.beanToMap(params));
		PersonalGjjInfoRsp response = new PersonalGjjInfoRsp();

		String spidNo = params.getSpidNo();
		String personalId = params.getPersonalId();

		if (StringUtils.isEmpty(spidNo) && StringUtils.isEmpty(personalId)) {
			response.setCode(BizResponseCode.PARAM_NOTNULL.getCode());
			response.setMessage(BizResponseCode.PARAM_NOTNULL.getMessage());
			return response;
		}

		PersonalGjjInfos pginfo=new PersonalGjjInfos();
		pginfo.setPlatformId(params.getPlatformId());
		if (!StringUtils.isEmpty(spidNo)) {
			pginfo.setSpidNo(params.getSpidNo());
		}
		if (!StringUtils.isEmpty(personalId)){
			pginfo.setPersonalId(params.getPersonalId());
		}
		PersonalGjjInfos pginfos = personalGjjMapper.selectOne(pginfo);
		if (null == pginfos) {//查询用户信息不存在
			response.setCode(BizResponseCode.USERINFO_NOTEXIST.getCode());
			response.setMessage(BizResponseCode.USERINFO_NOTEXIST.getMessage());
			return response;
		}
			response.setPersonalId(pginfos.getPersonalId());
			response.setPlatformId(pginfos.getPlatformId());
			response.setSpname(pginfos.getSpname());
			response.setSex(pginfos.getSex());
			response.setSpidNo(pginfos.getSpidNo());
			response.setZhzt(pginfos.getZhzt());
			response.setSjh(pginfos.getSjh());
			response.setOrgName(pginfos.getOrgName());
			response.setSnCode(pginfos.getSnCode());
			response.setSnName(pginfos.getSnName());
			response.setSpCode(pginfos.getSpCode());
			response.setKhrq(pginfos.getKhrq());
			response.setSpgz(pginfos.getSpgz());
			response.setSpsingl(pginfos.getSpsingl());
			response.setSpjcbl(pginfos.getSpjcbl());
			response.setSpmfact(pginfos.getSpmfact());
			response.setSpmend(pginfos.getSpmend());
			response.setSpjym(pginfos.getSpjym());	
			
			response.setCode(BizResponseCode.OK.getCode());
			response.setMessage(BizResponseCode.OK.getMessage());
			logger.info("修改用户信息返回参数：{}" + EntityUtils.beanToMap(response));
			return response;
			
	}
	
}
