package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.entity.PlatformCoinCustomer;
import me.flyray.crm.core.biz.platform.PlatformCoinCustomerBiz;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformCoinCustomer")
public class PlatformCoinCustomerController extends BaseController<PlatformCoinCustomerBiz, PlatformCoinCustomer> {

}