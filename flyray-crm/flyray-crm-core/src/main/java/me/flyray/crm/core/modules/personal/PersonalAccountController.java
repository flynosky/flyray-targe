package me.flyray.crm.core.modules.personal;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.core.entity.PersonalAccount;
import me.flyray.crm.facade.request.PersonalAccountRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalAccountBiz;

@RestController
@RequestMapping("personalAccount")
public class PersonalAccountController extends BaseController<PersonalAccountBiz, PersonalAccount> {
	/**
	 * 查询个人账户信息列表
	 * @author centerroot
	 * @time 创建时间:2018年8月15日上午10:32:02
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid PersonalAccountRequest personalAccountRequest){
		personalAccountRequest.setPlatformId(setPlatformId(personalAccountRequest.getPlatformId()));
		return baseBiz.queryList(personalAccountRequest);
	}
}