package me.flyray.gate.config;

import java.util.List;
import java.util.Map;

import me.flyray.gate.feign.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by xujingfeng on 2017/4/1.
 */
@Configuration
public class IpListConfig implements CommandLineRunner{
	public final static Logger logger = LoggerFactory.getLogger(IpListConfig.class);
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private IUserService userService;
	@Override
	public void run(String... args) throws Exception {
		logger.info("加载白名单开始");
		List<Map<String ,Object>> redisIplist = (List<Map<String, Object>>) redisTemplate.opsForValue().get("i_flyray-admin:ipList");
    	if(null == redisIplist || redisIplist.size() < 0 ){
			Map<String, Object>baseIpLists =userService.getIpList();
			List<Map<String ,Object>>ipList=(List<Map<String, Object>>) baseIpLists.get("ipList");
    		redisTemplate.opsForValue().set("i_flyray-admin:ipList", ipList);
    		//List<Map<String ,Object>>redisIplist=(List<Map<String, Object>>) redisTemplate.opsForValue().get("ipList");
    	}
    	logger.info("加载白名单结束");
	}

}
