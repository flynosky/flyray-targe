package me.flyray.admin.rpc.service;

import me.flyray.admin.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.admin.biz.UserBiz;

/** 
* @author: bolei
* @date：2018年4月17日 上午9:51:11 
* @description：类说明
*/

@Service
public class UserService {

	@Autowired
    private UserBiz userBiz;
	
	/**
	 * 新增用户 
	 * @param entity
	 */
	public int addUser(User entity) {
		return userBiz.insertSelective(entity);
    }
}
