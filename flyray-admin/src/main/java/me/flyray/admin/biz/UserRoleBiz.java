package me.flyray.admin.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.admin.entity.UserRole;
import me.flyray.admin.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.EntityUtils;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:17:59 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class UserRoleBiz extends BaseBiz<UserRoleMapper, UserRole> {

	public List<UserRole> getUserRolesByUserId(Long id) {
		return mapper.getUserRolesByUserId(id);
	}
	public Map<String, Object> add(Map<String, Object> param) throws Exception{
		UserRole userRole = EntityUtils.map2Bean(param, UserRole.class);
		mapper.insert(userRole);
		Map<String, Object> result = new HashMap<String, Object>();
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("message", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	result.put("userRoleId", userRole.getId());
    	return result;
	}
}
