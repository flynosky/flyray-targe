package me.flyray.admin.rest;

import me.flyray.admin.entity.Config;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.admin.biz.ConfigBiz;
import me.flyray.common.rest.BaseController;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/
@RestController
@RequestMapping("config")
public class ConfigController extends BaseController<ConfigBiz, Config> {

}
